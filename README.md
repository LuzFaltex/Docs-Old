![Build Status](https://gitlab.com/pages/mkdocs/badges/master/build.svg)

# LuzFaltex Docs

Thank you for your interest in contributing to the LuzFaltex docs. The LuzFaltex docs provide all of the documentation necessary for setting up and maintaining various products of LuzFaltex, as well as other miscelaneous items. This project has adopted the [Microsoft Open Source Code of
Conduct](https://opensource.microsoft.com/codeofconduct/).
For more information see the [Code of Conduct
FAQ](https://opensource.microsoft.com/codeofconduct/faq/) or
contact [opencode@microsoft.com](mailto:opencode@microsoft.com)
with any additional questions or comments.

# Contribute to LuzFaltex technical documentation

We welcome contributions from our community (users, customers, partners, LuzFaltex employees, and more). What you contribute is entirely up to you, but how you contribute may change depending on the scale of the change.

- **Minor Contributions**: Minor contributions, including updating links or images, spelling corrections, or adding code snippets can be submitted through a Merge Request to the `master` branch.

- **Major Contributions**: Major contributions include adding or removing pages, migrating pages, adding links to the navigation bar, or major restyling of an existing page should be submitted through a Merge Request to the `staging` branch.

## Branches

- The `master` branch is the branch from which the live site is rendered. Any changes merged here are visible as soon as the site is rebuilt, which happens usually within five minutes of a change being pushed.

- The `staging` branch is a branch dedicated to testing large changes before they go live. Once they have passed testing, changes will be pushed to the master branch.

## Contribution Guidelines

Any contributions you submit for documentation and code examples in this repo are covered by the [docs.luzfaltex.com Terms of Use](./Docs/Legal/tos.md).
